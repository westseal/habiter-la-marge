## 24/05/2022

- Différencier les actions collectives synchrones (le collectif est une condition de la réalisation de l'action) du partage asynchrone. 
- Deux grandes parties : les dispositifs de partage de connaissances et les dispositifs de documentation en vue de ce partage.

mots clés :

- documenter
- archiver
- participer
- construire
- communs
- connaissances
- savoirs
- station des savoirs
- in situ
- local
- outils -> qu'est-ce qu'un outil ? Ces systèmes sont-ils des outils ? De quels outils se composent-ils ?

culture numérique du partage
culture du bricolage, DIY, fablab, hacker dans l'espace public

Historique des moyens de documentation ?

Récupedia un peu

https://gaite-lyrique.net/evenement/copy-party

Ok c'est très flou -> faire une carte mentale 


-> Dimension locale et in-situ : rapport au territoire, relation entre les humain·es et les non-humain·es. 
-> Une façon d'habiter le monde 
-> Initiatives qui apparaissent lors d'une ouverture, d'une brèche, occupation, évènement

