
# Journal de bord

## 24/05/2022 au 27/05/2022

Mise en place des premiers outils pour entamer le projet de recherche.

- Site présent réalisé avec Mkdocs permettant la documentation de la recherche.
  
- Premiers pas sur l'outil Tropy de tri et de classement d'images. Mise en place d'un système de récupération des données du logiciel pour les mettre en forme sous une page web.


## 5/06/2022

- Récupération des données issues de ma bilbiothèque Zotero et construction de l'interface.