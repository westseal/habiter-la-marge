# Ressources à explorer

## Films
- Claire Simon, Le Bois dont les rêves sont faits, 2015

## Artistes    
- Kader Attia

## Littérature
- Kantuta Quirós, Aliocha Imhoff et Camille de Toledo, *Les Potentiels du temps*, 2016
- Emmanuel Hocquard, Dernières nouvelles de la cabane, 1998
- Deborah Bird Rose, *Vers des humanités écologiques*, 2019
- Catherine Larrère, Raphaël Larrère, *Bulles technologiques*, 2017
- Yvan Detraz, *La marche comme projet urbain*, 2020
- Aldo Leopold, *La terre comme communauté*, 2021
- Marin Schaffner, *Un seul commun, Lutter, habiter, penser*, 2019
- Jade Lindgaard, *Éloge des mauvaises herbes, ce que nous devons à la ZAD*, 2018
- Anna Tsing, *Le Champignon de la fin du monde*. Sur la possibilité de vie
dans les ruines du capitalisme, trad. Philippe Pignarre, La Découverte,
2017.
- Gilles Clément, *Le Jardin en mouvement*, Pandora, 1991.
- Gilles Clément,*Manifeste du tiers paysage*, Sujet-objet, 2004 (rééd. Éditions du
Commun, 2016, en ligne).
- Sébastien Thiéry (dir.), *Des Actes. À Calais, et tout autour* Post Éditions,
- Michel Naepels, *Dans la détresse. Une anthropologie de la vulnérabilité*,
Éditions de l’EHESS, 2019
- Nele Wynants, *When Fact is Fiction. Art in the Post-Truth Era*, 2020
- Ruebn Pater, *How capitalism took hold of graphic design and how to escape from it*, 2021
- Didelon, V. & al, *Voir l’architecture. Contribution du design à la construction des savoirs*.
B42, 2015
- P-D Huyghe, *Contre-temps. De la recherche et de ses enjeux. Arts, architecture, design*.
B42, 2017
- Olivier Le Deuff, Le Temps des humanités digitales : La mutation des sciences humaines et sociales, FYP Éditions, 175 p.

## Articles 

- Annette Béguin, « Information, Communication et Anthropologie des savoirs », REC IIS, vol. 3
- Yolande Maury, Susan Kovacs, « Étudier la part de l’humain dans les savoirs : les Sciences de l’information et de la communication au défi de l’anthropologie des savoirs », Études de communication, 42, 2014, p. 15-28. Disponible sur : http://edc.revues.org/5655

