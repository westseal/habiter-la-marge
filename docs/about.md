Ce site est en cours de construction. 
Il se base à ce stade sur mkdocs, un générateur de site statique en markdown. [https://www.mkdocs.org/]().

Les fichiers sont disponibles sur gitlab -> [https://gitlab.com/westseal]()

Il est temporairement hébergé par Vercel, (à défaut de réussir à utiliser Git pages sans devoir rentrer la carte bancaire).
