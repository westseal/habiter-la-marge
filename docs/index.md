# Habiter la marge : transmission des savoirs issus des expériences collectives alternatives 
## Projet de recherche sur les dispositifs auto-construits de transmission des savoirs 


> Dans les bas-côtés, au bord des routes, là où l'espace n'est pas figé, la végétation croît. Les fleurs poussent et leurs graines se propagent. Elles se nourissent de la limite et la nourissent en retour.

> "Mieux que des bords donc, mieux que délaissés et activement invisibilisés, des franges qui seraient des preuves, la preuve qu'on pourrait faire autrement puisqu'on fait autrement." Marielle Macé, 2017 : 67.

Ce site a pour objet la documentation de mon projet de recherche portant sur les **dispositifs et les systèmes de partage de connaissances in-situ initiées et construits par différent·es acteur·ices**. Mon intérêt se porte d'avantage sur les dispositifs qui mêlent à la physicalité de la matière une dimension numérique. Plus généralement, le projet s'insère dans la voie des communs de la connaissance et privilégie la recherche de "cabanes de savoirs" dans des lieux en marge, où la fabrication de ces systèmes participe au faire expérience du paysage. En ce sens, il est question de sortir la technique de son rôle de médiatrice des relations entre humain·es et non-humain·es pour la considérer pleinement en son sein. Il s'agit dès lors de participer à la construction d'un imaginaire à travers un questionnement sur l'esthétique de ces communs, c'est-à-dire à travers l'étude des formes, des récits et des images qui la composent. Comment ces initiatives émergent-elles ? En quoi la fabrication autonome de ces systèmes de partage de savoirs permet-elle de repenser nos façons d'habiter le monde, tant tangible que numérique ? Un détour auprès des systèmes de production des savoirs académique semble intéressant afin d'établir un horizon des processus de production et de diffusion de ces savoirs.


![](images/dispositif1.svg)

![](images/dispositif2.svg)

Dans le cadre d'une démarche en design, il s'agit de questionner les différents **gestes éditoriaux** impliqués dans la production des savoirs et des connaissances; du recueil des données jusqu'à l'objet qui les rassemble en portant un regard attentif aux **gestes sémantiques** qui les accompagnent. Collecter, inventorier, récolter, rassembler, cataloguer, recenser, relever, lister. Organiser, trier, hiérarchiser, classer, ranger, distribuer, ordonner, distinguer, répartir, grouper, démêler, catégoriser, lier, connecter, joindre, disjoindre. Mettre en forme, mettre en page, mettre en espace, agencer, disposer, arranger, composer, structurer. Archiver, activer, actionner, manipuler, performer.


